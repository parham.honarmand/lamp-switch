import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    static List added = new ArrayList();
    static int counter;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("please enter your numbers :");
        String[] inputs = scanner.nextLine().split(",");
        calculateNumberOfTurnOn(inputs);

        System.out.println(counter);
    }

    public static void calculateNumberOfTurnOn(String[] inputs) {
        int checkedPoint = 1;
        HERE:
        for (int i = 0; i < inputs.length; i++) {
            int num = Integer.parseInt(inputs[i]);
            added.add(num);
            for (int j = checkedPoint; j < num; j++)
                if (!added.contains(j))
                    continue HERE;

            checkedPoint = num;
            counter++;
        }
    }
}
